# cnc
Polar coordinates cnc firmware (based on Arduino MEGA + Ramps 1.4)

## Implemented Gcode instructions
- G0, G1 - linear move
- G2, G3 - clockwise and counter clockwise arc moves
- G4 - wait

## To be implemented
- G92 - set position
- code refactoring - divide into more files, line comments in EN
- feed rate calibration
- reverse kinematics calculation (reduce XY position drift)
- pulley diameter compensation
