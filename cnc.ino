#define X_STEP_PIN 54
#define X_DIR_PIN 55
#define X_ENABLE_PIN 38
#define X_MIN_PIN 3
#define X_MAX_PIN 2

#define Y_STEP_PIN 60
#define Y_DIR_PIN 61
#define Y_ENABLE_PIN 56
#define Y_MIN_PIN 14
#define Y_MAX_PIN 15

#define Z_STEP_PIN 46
#define Z_DIR_PIN 48
#define Z_ENABLE_PIN 62
#define Z_MIN_PIN 18
#define Z_MAX_PIN 19

#define GANTRY_WIDTH 1100
#define GANTRY_HEIGHT 450
#define X_STEPS_TO_MM 22
#define Y_STEPS_TO_MM 22
#define Z_STEPS_TO_MM 10
#define TIME_CONSTANT 100

boolean abs_mode=true;

float curX=0;
float curY=0;
float curZ=0;

float curLenX=0;
float curLenY=0;

float units=1;

float X=0;
float Y=0;
float Z=0;
float I=0;
float J=0;
float feedRate=100;

float calcX;
float calcY;
float calcZ;

float calcLenX=0;
float calcLenY=0;

void setup() {
  pinMode(X_STEP_PIN , OUTPUT);
  pinMode(X_DIR_PIN , OUTPUT);
  pinMode(X_ENABLE_PIN , OUTPUT);

  pinMode(Y_STEP_PIN , OUTPUT);
  pinMode(Y_DIR_PIN , OUTPUT);
  pinMode(Y_ENABLE_PIN , OUTPUT);

  pinMode(Z_STEP_PIN , OUTPUT);
  pinMode(Z_DIR_PIN , OUTPUT);
  pinMode(Z_ENABLE_PIN , OUTPUT);
  
  //inicializace
  calcKinematics(0,0,0);
  curLenX=calcLenX;
  curLenY=calcLenY;
  
  Serial.begin(115200);
  Serial.setTimeout(30);
}


void loop () {
  String instruction;
  if (Serial.available() > 0){
     instruction = Serial.readString();
     parseGcode(instruction,instruction.length());
  }
}

void parseGcode(String instruction,int size){
  char temp_word[2] = {instruction[1], instruction[2]};
  int word = 0;
  if (instruction[0] == 'G'){
    word = atoi(temp_word);
  }

  switch (word) {
    case 00:
    case 01:
       //linear moves
       if(!abs_mode){
         X += (search_string('X', instruction, size));
         Y += (search_string('Y', instruction, size));
         Z += (search_string('Z', instruction, size));
       }else{
         X = (search_string('X', instruction, size));
         Y = (search_string('Y', instruction, size));
         Z = (search_string('Z', instruction, size));       
       }

       feedRate = (search_string('F', instruction, size));

       moveXYZ();
       instruction = NULL;
       
       Serial.print("X: ");
       Serial.print(X);
       Serial.print(" Y: ");
       Serial.print(Y);
       Serial.print(" Z: ");
       Serial.print(Z);
       Serial.println("ok");
       
       break;
       
    case 02:
    case 03:
       //arc moves
       if(!abs_mode){
         X += (search_string('X', instruction, size));
         Y += (search_string('Y', instruction, size));
         Z += (search_string('Z', instruction, size));
         I += (search_string('I', instruction, size));
         J += (search_string('J', instruction, size));
       }else{
         X = (search_string('X', instruction, size));
         Y = (search_string('Y', instruction, size));
         Z = (search_string('Z', instruction, size));
         I = (search_string('I', instruction, size));
         J = (search_string('J', instruction, size));
       }

       feedRate = (search_string('F', instruction, size));

       moveXYZIJ(word==3?true:false);
       
       instruction = NULL;
       
       Serial.print("X: ");
       Serial.print(X);
       Serial.print(" Y: ");
       Serial.print(Y);
       Serial.print(" Z: ");
       Serial.print(Z);
       Serial.println("ok");
       
       break;
       
    case 04:
       delay((int)(search_string('P', instruction, size)));
       Serial.println("ok");
       break;
  
    case 91:
      abs_mode = true;
      Serial.println("ok");
      break;
 
    case 92:
      abs_mode = false;
      Serial.println("ok");
      break;

  default:
     Serial.print("WARN: unknown instruction - "); 
     Serial.println(instruction);
  }
}


double search_string(char key, String instruction, int string_size){
  char result[string_size];
  int i=0;
  while(i<string_size){
    i++;
    if (instruction[i] == key){
      i++;
      int k = 0;
      while (instruction[i] != ' ' && instruction[i]!='\r' && instruction[i]!='\n' && instruction[i]!=NULL){
        result[k] = instruction[i];
        i++; k++;
      }
      return atof(result);
    }
  }
  
  //coordinates not found, set original
  if(key=='X'){
    return X;
  }
  if(key=='Y'){
    return Y;
  }
  if(key=='Z'){
    return Z;
  }
  if(key=='F'){
    return feedRate;
  }
  if(key=='I'){
    return I;
  }
  if(key=='J'){
    return J;
  }
}

void moveXYZIJ(boolean reverse){
  long iterations = TIME_CONSTANT*countSteps()/feedRate;
  long i=1;
  
  float startX=curX;
  float startY=curY;
  float startZ=curZ;
  
  //calc radius
  float r=sqrt(pow(I,2)+pow(J,2));
  
  //scalar vector product, deg difference
  float startDeg=acos(-I/(sqrt(pow(I,2)+pow(J,2))));
  float endDeg=acos((X-startX-I)/(sqrt(pow((X-startX-I),2)+pow((Y-startY-J),2))));
  float diff=(endDeg-startDeg);

  while(i<=iterations){
   
    //kinematics forward
    float phi;
    if(reverse){
      phi=startDeg+(((endDeg-startDeg)*i)/iterations);
    }else{
      phi=startDeg+(((endDeg-startDeg)*i)/iterations);
    }
    float targetX=startX+I+r*cos(phi);
    float diffY=r*sin(phi);
    float targetY=startY+J+(diffY*(startDeg<endDeg?-1:1)*(reverse?-1:1));

    //Serial.print(diffY);Serial.print(";");
    //Serial.print(targetX);Serial.print(";");
    //Serial.print(targetY);Serial.println(";");
    
    float targetZ=startZ+(i*((Z-startZ)/iterations));
       
    calcKinematics(targetX,targetY,targetZ);

    //calculate delta of lengths
    float deltaLenX=calcLenX-curLenX;
    float deltaLenY=calcLenY-curLenY;
    float deltaZ=calcZ-curZ;
    
    //calculate number of steps
    int stepsX=round(deltaLenX*(float)X_STEPS_TO_MM);
    int stepsY=round(deltaLenY*(float)Y_STEPS_TO_MM);
    int stepsZ=round(deltaZ*(float)Z_STEPS_TO_MM);

    deltaLenX=(float)stepsX/(float)X_STEPS_TO_MM;
    deltaLenY=(float)stepsY/(float)Y_STEPS_TO_MM;
    deltaZ=(float)stepsZ/(float)Z_STEPS_TO_MM;

    //set lengths
    curLenX=curLenX+deltaLenX;
    curLenY=curLenY+deltaLenY;
    curZ=curZ+deltaZ;
        
    moveSteps(stepsX, stepsY, stepsZ);
    
    i++;
  }
  //to be verified!!!!!
  //calcInverseKinematics(curLenX,curLenY,curZ);
  //setnutí true koordinátů
  curX=X;
  curY=Y;
}

void moveXYZ(){

  long iterations = TIME_CONSTANT*countSteps()/feedRate;
  long i=1;
  
  float startX=curX;
  float startY=curY;
  float startZ=curZ;
  
  while(i<=iterations){
   
    //kinematics
    float targetX=startX+(i*((X-startX)/iterations));
    float targetY=startY+(i*((Y-startY)/iterations));
    float targetZ=startZ+(i*((Z-startZ)/iterations));
    
    calcKinematics(targetX,targetY,targetZ);

    //calculate delta of lengths
    float deltaLenX=calcLenX-curLenX;
    float deltaLenY=calcLenY-curLenY;
    float deltaZ=calcZ-curZ;
    
    //calculate number of steps
    int stepsX=round(deltaLenX*(float)X_STEPS_TO_MM);
    int stepsY=round(deltaLenY*(float)Y_STEPS_TO_MM);
    int stepsZ=round(deltaZ*(float)Z_STEPS_TO_MM);

    deltaLenX=(float)stepsX/(float)X_STEPS_TO_MM;
    deltaLenY=(float)stepsY/(float)Y_STEPS_TO_MM;
    deltaZ=(float)stepsZ/(float)Z_STEPS_TO_MM;

    //set lengths
    curLenX=curLenX+deltaLenX;
    curLenY=curLenY+deltaLenY;
    curZ=curZ+deltaZ;
        
    moveSteps(stepsX, stepsY, stepsZ);
    
    i++;
  }
  //calcInverseKinematics(curLenX,curLenY,curZ);

  //setnutí true koordinátů
  curX=X;
  curY=Y;
}

void moveSteps(int stepsX,int stepsY,int stepsZ){
 
  if(stepsX>0){
    digitalWrite(X_DIR_PIN , HIGH);  
  }else{
    digitalWrite(X_DIR_PIN , LOW);
  }
 
  int j=0;
  while(j<abs(stepsX)){
    digitalWrite(X_STEP_PIN , LOW);
    delayMicroseconds(100); 
    digitalWrite(X_STEP_PIN , HIGH);
    delayMicroseconds(100);
    j++;
  }  
  
  if(stepsY>0){
    digitalWrite(Y_DIR_PIN , HIGH);  
  }else{
    digitalWrite(Y_DIR_PIN , LOW);
  }
  
  j=0;
  while(j<abs(stepsY)){
    digitalWrite(Y_STEP_PIN , HIGH);
    delayMicroseconds(100);
    digitalWrite(Y_STEP_PIN , LOW);
    delayMicroseconds(100); 
    j++;
  }

  if(stepsZ>0){
    digitalWrite(Z_DIR_PIN , HIGH);  
  }else{
    digitalWrite(Z_DIR_PIN , LOW);
  }

  j=0;
  while(j<abs(stepsZ)){
    digitalWrite(Z_STEP_PIN , HIGH);
    delayMicroseconds(100);
    digitalWrite(Z_STEP_PIN , LOW);
    delayMicroseconds(100); 
    j++;
  }
}

long countSteps(){
  calcKinematics(curX,curY,curZ);
  long steps=0;
  float tmpLenX=calcLenX;
  float tmpLenY=calcLenY;
  float tmpZ=calcZ;
  calcKinematics(X,Y,Z);
  return (long) (abs(tmpLenX-calcLenX)*X_STEPS_TO_MM) + (abs(tmpLenY-calcLenY)*Y_STEPS_TO_MM) + (abs(tmpZ-calcZ)*Z_STEPS_TO_MM);  
}

void calcKinematics(float X, float Y, float Z){
  calcLenX=sqrt(pow((GANTRY_WIDTH/2)+X,2)+pow(GANTRY_HEIGHT-Y,2));
  calcLenY=sqrt(pow((GANTRY_WIDTH/2)-X,2)+pow(GANTRY_HEIGHT-Y,2));
  calcZ=Z;
}

void calcInverseKinematics(float lenX, float lenY, float Z){
  calcY = sqrt((-GANTRY_WIDTH-lenX-lenY)*(GANTRY_WIDTH-lenX-lenY)*(GANTRY_WIDTH+lenX-lenY)*(GANTRY_WIDTH-lenX+lenY))/(2*GANTRY_WIDTH);
  calcX = GANTRY_HEIGHT-sqrt((lenY*lenY)-(calcY*calcY));
  calcY = (GANTRY_WIDTH/2)-calcY;
  calcZ=Z;
}



